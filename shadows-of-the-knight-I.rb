STDOUT.sync = true

w, h = gets.split(" ").collect { |x| x.to_i }
n = gets.to_i
x0, y0 = gets.split(" ").collect { |x| x.to_i }

minX, minY, maxX, maxY = 0, 0, w - 1, h - 1
loop do
  bomb_dir = gets.chomp
  STDERR.puts bomb_dir
  if bomb_dir.include?("U")
    maxY = y0 - 1
  elsif bomb_dir.include?("D")
    minY = y0 + 1
  end
  if bomb_dir.include?("L")
    maxX = x0 - 1
  elsif bomb_dir.include?("R")
    minX = x0 + 1
  end

  x0 = minX + ((maxX - minX) / 2).ceil()
  y0 = minY + ((maxY - minY) / 2).ceil()
  puts "#{x0} #{y0}"
end
