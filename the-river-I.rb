r_1 = gets.to_i
r_2 = gets.to_i

while r_1 != r_2
    if r_1 < r_2
        r_1 += r_1.digits.sum
    else
        r_2 += r_2.digits.sum
    end
end

puts r_1
