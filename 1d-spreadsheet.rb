$vars = []
operations = []
n = gets.to_i

def actual(value)
  return value == "_" ? 0 : value[0] == '$' ? $vars[value[1..-1].to_i] : value.to_i
end

def compute(op, v1, v2)
  if v1 == nil || v2 == nil
    return nil
  end
  res = v1
  case op
  when "ADD"
    res += v2
  when "SUB"
    res -= v2
  when "MULT"
    res *= v2
  end
  return res
end

n.times do
  line = gets.chomp
  operation, arg_1, arg_2 = line.split(" ")
  $vars << compute(operation, actual(arg_1), actual(arg_2))
  operations << line
end

while true
  for i in 0..n-1 do
    operation, arg_1, arg_2 = operations[i].split(" ")
    if actual(arg_1) != nil && (actual(arg_2) != nil || operation == "VALUE")
      value = compute(operation, actual(arg_1), actual(arg_2))
      $vars[i] = value
    end
  end
  if !$vars.include? nil
    break
  end
end

for i in 0..n-1 do
  puts $vars[i]
end
